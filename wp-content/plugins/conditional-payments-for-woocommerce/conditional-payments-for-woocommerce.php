<?php

/*
Plugin Name: Conditional Payments for WooCommerce
Description: Disable payment methods based on shipping methods, customer address and much more.
Version:     2.2.2
Author:      Lauri Karisola / WooElements.com
Author URI:  https://wooelements.com
Text Domain: woo-conditional-payments
Domain Path: /languages
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
WC requires at least: 3.0.0
WC tested up to: 4.7.1
*/

/**
 * Prevent direct access to the script.
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Plugin version
 */
if ( ! defined( 'CONDITIONAL_PAYMENTS_FOR_WOO_VERSION' ) ) {
	define( 'CONDITIONAL_PAYMENTS_FOR_WOO_VERSION', '2.2.2' );
}

/**
 * Assets version
 */
if ( ! defined( 'WOO_CONDITIONAL_PAYMENTS_ASSETS_VERSION' ) ) {
	define( 'WOO_CONDITIONAL_PAYMENTS_ASSETS_VERSION', '2.2.2' );
}

/**
 * Load plugin textdomain
 *
 * @return void
 */
add_action( 'plugins_loaded', 'woo_conditional_payments_load_textdomain' );
function woo_conditional_payments_load_textdomain() {
  load_plugin_textdomain( 'woo-conditional-payments', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );
}

class Woo_Conditional_Payments {
	function __construct() {
		// WooCommerce not activated, abort
		if ( ! defined( 'WC_VERSION' ) ) {
			return;
		}

		// Prevent running same actions twice if Pro version is enabled
		if ( class_exists( 'Woo_Conditional_Payments_Pro' ) ) {
			return;
		}

		if ( ! defined( 'WOO_CONDITIONAL_PAYMENTS_BASENAME' ) ) {
			define( 'WOO_CONDITIONAL_PAYMENTS_BASENAME', plugin_basename( __FILE__ ) );
		}

		$this->includes();
	}

	/**
	 * Include required files
	 */
	public function includes() {
		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/class-woo-conditional-payments-updater.php' );

		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/class-conditional-payments-filters.php' );

		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/class-woo-conditional-payments-post-type.php', 'Woo_Conditional_Payments_Post_Type' );

		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/class-woo-conditional-payments-ruleset.php', 'Woo_Conditional_Payments_Ruleset' );

		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/woo-conditional-payments-utils.php' );

		if ( is_admin() ) {
			$this->admin_includes();
		}

		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/frontend/class-woo-conditional-payments-frontend.php', 'Woo_Conditional_Payments_Frontend' );
	}

	/**
	 * Include admin files
	 */
	private function admin_includes() {
		$this->load_class( plugin_dir_path( __FILE__ ) . 'includes/admin/class-woo-conditional-payments-admin.php', 'Woo_Conditional_Payments_Admin' );
	}

	/**
	 * Load class
	 */
	private function load_class( $filepath, $class_name = FALSE ) {
		require_once( $filepath );

		if ( $class_name ) {
			return new $class_name;
		}

		return TRUE;
	}
}

function init_woo_conditional_payments() {
	new Woo_Conditional_Payments();
}
add_action( 'plugins_loaded', 'init_woo_conditional_payments', 110 );

