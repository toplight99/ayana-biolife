<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

class WOOMULTI_CURRENCY_Plugin_Compatible_3rd_Plugins {

	public function __construct() {
		add_filter( 'wmc_3rd_change_price', array( $this, 'change_price_for_3rd_plugins' ) );
	}

	public function change_price_for_3rd_plugins( $price ) {
		return wmc_get_price( $price );
	}
}

