<section>
    <div class="container">
        <h2 class="section-title text-center"><?php echo get_sub_field('title'); ?></h2>
        <?php echo do_shortcode(get_sub_field('shortcode')); ?>
    </div>
</section>

