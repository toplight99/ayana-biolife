<?php
    $slides = get_sub_field('items');
?>
<section class="slider-images">
    <div class="owl-carousel owl-theme">
        <?php foreach($slides as $slide): ?>
            <div class="slide-item" style="background-image: url('<?php echo $slide['image']; ?>')">
                <div class="container">
                    <div class="slide-item-content">
                        <div class="title"><?php echo $slide['title']; ?></div>
                        <div class="subtitle"><?php echo $slide['subtitle']; ?></div>
                        <a class="link" href="<?php echo get_permalink($slide['link']['page']); ?>"><?php echo $slide['link']['title']; ?></a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</section>