<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package NDD
 */

?>

<div class="col-md-4">
    <article class="blog-item p0">
        <div class="blog-item-image">
            <div class="blog-item-image-src" style="background-image: url('<?php echo get_the_post_thumbnail_url(get_the_ID()) ?>');">

            </div>
            <div class="blog-item-date"><i class="far fa-calendar"></i> <?php echo get_the_date(); ?></div>
        </div>
        <div class="blog-item-title">
            <?php the_title( '<h3><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' ); ?>
        </div>
        <div class="blog-item-text"><?php the_excerpt(); ?></div>
        <div class="blog-item-button"><a href="<?php the_permalink(); ?>" class="btn btn-primary-1">EN SAVOIR PLUS</a></div>
    </article>
</div>