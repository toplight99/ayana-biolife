<?php
/**
 * Template Name: Page Builder
 *
 */

get_header();
the_post();
have_posts();

while ( have_posts() ) : the_post();
    if( have_rows('content') ):
        while ( have_rows('content') ) : the_row();
            include(locate_template('inc/acf/page-builder/content/'.get_row_layout().'.inc'));
        endwhile;
    endif;
endwhile;

get_footer();
