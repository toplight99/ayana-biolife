<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>

	<header class="site-header" role="banner" style="<?php storefront_header_styles(); ?>">
        <div class="container-fluid">
            <?php storefront_site_title_or_logo(); ?>

            <div class="storefront-primary-navigation">
                <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Navigation', 'storefront' ); ?>">
                    <button class="menu-toggle" aria-controls="site-navigation" aria-expanded="false"><span><?php echo esc_html( apply_filters( 'storefront_menu_toggle_text', __( 'Menu', 'storefront' ) ) ); ?></span></button>

                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location'  => 'primary',
                            'container_class' => 'primary-navigation',
                        )
                    );

                    wp_nav_menu(
                        array(
                            'theme_location'  => 'primary',
                            'container_class' => 'handheld-navigation',
                        )
                    );
                ?>
                </nav>
            </div>

            <?php storefront_header_cart(); ?>

        </div>
	</header>

    <?php do_action( 'storefront_content_top' ); ?>
