<?php
/**
 * Template Name: Steps
 *
 */

get_header();
the_post();
have_posts();

?>
    <div class="page-content">
        <?php the_title('<h1 class="text-center">', '</h1>')?>

        <div class="container text-center">
            <?php the_content(); ?>

            <?php
            $steps = get_field('steps');
            ?>
            <?php if($steps): ?>
                <div class="ayana-steps">
                    <?php foreach ($steps as $key => $step): ?>
                        <div class="ayana-step">
                            <div class="step-icon">
                                <i class="c-content-line-icon c-theme <?php echo $step['icon']?>"></i>
                            </div>
                            <div class="step-content">
                                <div class="step-ttile"><span><?php echo $key+1; ?>.</span> <?php echo $step['title']?></div>
                                <div class="step-description"><?php echo $step['description']?></div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

<?php

get_footer();
