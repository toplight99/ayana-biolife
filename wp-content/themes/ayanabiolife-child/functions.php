<?php

if( !defined('ENV_MODE') || ENV_MODE != 'dev'){
    include('inc/acf/fields.inc');
    add_filter('acf/settings/show_admin', '__return_false');
}

function wp_ayana_scripts() {
    wp_enqueue_style( 'wp-ayana-bootstrap-css', get_theme_file_uri() . '/inc/assets/libs/bootstrap/css/bootstrap.min.css' );
    wp_enqueue_style( 'wp-ayana-fontawesome-cdn', get_theme_file_uri() . '/inc/assets/css/fontawesome.min.css' );
    wp_enqueue_style( 'wp-owlcarousel-css', get_theme_file_uri() . '/inc/assets/libs/owlcarousel/assets/owl.carousel.min.css' );
    wp_enqueue_style( 'wp-owlcarousel-theme-css', get_theme_file_uri() . '/inc/assets/libs/owlcarousel/assets/owl.theme.default.min.css' );

    wp_enqueue_style( 'wp-google-font-Montserrat', 'https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap' );

    wp_enqueue_style( 'wp-ayana-style', get_stylesheet_uri() );

    wp_enqueue_script('jquery');

    // Internet Explorer HTML5 support
    wp_enqueue_script( 'html5hiv',get_theme_file_uri().'/inc/assets/js/html5.js', array(), '3.7.0', false );
    wp_script_add_data( 'html5hiv', 'conditional', 'lt IE 9' );
    wp_enqueue_script('wp-owlcarousel-js', get_theme_file_uri() . '/inc/assets/libs/owlcarousel/owl.carousel.min.js', array(), '', true );

    wp_enqueue_script('wp-ayana-popper', get_theme_file_uri() . '/inc/assets/js/popper.min.js', array(), '', true );
    wp_enqueue_script('wp-ayana-bootstrapjs', get_theme_file_uri() . '/inc/assets/libs/bootstrap/js/bootstrap.min.js', array(), '', true );

    wp_enqueue_script('wp-ayana-script', get_theme_file_uri() . '/inc/assets/js/script.js', array(), '', true );


    if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
        wp_enqueue_script( 'comment-reply' );
    }
}
add_action( 'wp_enqueue_scripts', 'wp_ayana_scripts' );

add_action( 'get_header', 'remove_storefront_sidebar' );
function remove_storefront_sidebar() {
    //if ( is_woocommerce() ) {
        remove_action( 'storefront_sidebar', 'storefront_get_sidebar', 10 );
    //}
}

add_action( 'wp', 'bbloomer_remove_default_sorting_storefront' );

function bbloomer_remove_default_sorting_storefront() {
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_catalog_ordering', 10 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );
}

add_action( 'init', 'bbloomer_delay_remove_result_count' );

function bbloomer_delay_remove_result_count() {
    remove_action( 'woocommerce_after_shop_loop', 'woocommerce_result_count', 20 );
    remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
}

function woocommerce_shortcode_display_all_products($args)
{
    if(strtolower(@$args['post__in'][0])=='all')
    {
        global $wpdb;
        $args['post__in'] = array();
        $products = $wpdb->get_results("SELECT ID FROM ".$wpdb->posts." WHERE `post_type`='product'",ARRAY_A);
        foreach($products as $k => $v) { $args['post__in'][] = $products[$k]['ID']; }
    }
    return $args;
}
add_filter('woocommerce_shortcode_products_query', 'woocommerce_shortcode_display_all_products');

add_filter('loop_shop_columns', 'loop_columns', 999);
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 4;
    }
}

if (!function_exists('woocommerce_template_loop_add_to_cart')) {
    function woocommerce_template_loop_add_to_cart() {
        global $product;
        if ( ! $product->is_in_stock() || ! $product->is_purchasable() || 'variable' === $product->product_type ) return;
        wc_get_template('loop/add-to-cart.php');
    }
}

if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Ayana Biolife',
        'menu_title'	=> 'Ayana Biolife',
    ));
}

//foreach (['en', 'fr'] as $lang) {
//    acf_add_options_sub_page([
//        'page_title' => "Ayana Biolife ${$lang}",
//        'menu_title' => __("Ayana Biolife ${$lang}", 'storefront'),
//        'menu_slug' => "ayana-biolife-${lang}",
//        'post_id' => $lang
//    ]);
//}


add_filter('woocommerce_currency_symbol', 'my_currency_symbol', 10, 2);



function my_currency_symbol( $currency_symbol, $currency ) {
    switch( $currency ) {
        case 'DHS': $currency_symbol = 'DHS';
            break;
    }
    return $currency_symbol;
}


add_filter( 'woocommerce_product_description_heading', '__return_null' );


add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
function jk_related_products_args( $args ) {
    $args['posts_per_page'] = 4;
    $args['columns'] = 4;
    return $args;
}

add_filter( 'wc_product_sku_enabled', '__return_false' );


