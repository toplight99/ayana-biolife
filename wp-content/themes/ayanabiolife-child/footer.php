<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="logo">
                    <img src="<?php the_field('logo_footer', 'options'); ?>" alt="">
                </div>

            </div>
            <div class="col-12 col-lg-4">
                <div class="company-info">
                    <div class="address">
                        <i class="fas fa-map-marker-alt"></i>
                        <?php the_field('address', 'options'); ?><br>
                        <?php the_field('postal_code', 'options'); ?>
                        <?php the_field('city', 'options'); ?>,
                        <?php the_field('country', 'options'); ?>
                    </div>
                    <div class="phones">
                        <i class="fas fa-phone-alt"></i> <a href="tel:<?php the_field('phone', 'options'); ?>"><?php the_field('phone', 'options'); ?></a><br>
                        <i class="fas fa-phone-alt"></i> <a href="tel:<?php the_field('phone_2', 'options'); ?>"><?php the_field('phone_2', 'options'); ?></a>
                    </div>
                    <div class="email">
                        <i class="fas fa-envelope"></i> <a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a><br>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <?php
                    wp_nav_menu(
                        array(
                            'theme_location'  => 'secondary',
                            'container_class' => 'footer-navigation',
                        )
                    );
                ?>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-7">
                    <div class="copyright-text">
                        © 2021 Copyright <a href="<?php echo home_url(); ?>"><?php the_field('name', 'options'); ?></a>. All rights reserved
                    </div>
                </div>
                <div class="col-md-5">
                    <?php
                        $social_media = get_field('social_media', 'options');
                    ?>
                    <?php if($social_media): ?>
                    <div class="social-media">
                        <?php foreach ($social_media as $item): ?>
                        <a href="<?php echo $item['url']; ?>" target="_blank" rel="noopener noreferrer"><i class="<?php echo $item['icon']; ?>"></i></a>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</footer>



<?php wp_footer(); ?>

</body>
</html>
