<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'ayanabiolife-3' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'e#6zj/K-yNRHz7$U}x@/I7r{zbM,T%0Lr_!clqy/?pZGBfeln5n*?Pdd.TqHvZtN' );
define( 'SECURE_AUTH_KEY',  '#,GqMSIiXMiO8VRfgh.COB@P?LCqCBcI;Dr.%TsPkUA1[85FoeGG|m3cHLV9+EUI' );
define( 'LOGGED_IN_KEY',    'b5<}$cC5+un?g^fi?}SEu18C?b?NC@lH.?jhyB?`RW&`u&X+]:V@4IK+DXkl@3e]' );
define( 'NONCE_KEY',        'j1K*-zx#K2uIXF)lIJwl-=FSOus)*&!,c~XXo(E01g 1^(F)%YnQwB3xIHj/<wH7' );
define( 'AUTH_SALT',        '%nWUh=@*>D/Y>ea-MNad^~OzyR~I`0L=M!.x[d!!ZNhf6.-PTTqU>QB1JC$pI7^&' );
define( 'SECURE_AUTH_SALT', 'OMQ0Be.&U}4YR!#OU[f9e}GhjINKI`M<IYR);*;7Zh(lRWbGu)Dg[G:zb+~|lU$U' );
define( 'LOGGED_IN_SALT',   '&LbW_MM,-{UR@4We*RcC8 1x1tdG!].qKW*nyIC&#pyfe-;V7GJtC-S%oyp7W*sx' );
define( 'NONCE_SALT',       'ZZ_50E#;ntlQ?#-LI5.zmQf/cnnwyMhRa@}>)mwmB,yr~:;gA%c3D;uCT.WKrnvu' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'vlrlzkpl';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

define('ENV_MODE', 'dev');

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
